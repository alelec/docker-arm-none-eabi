--------------------
Docker arm-none-eabi
--------------------

Docker image for use in CI builds of bare metal arm projects with the official ARM gcc toolchain.

More info at:
* https://developer.arm.com/open-source/gnu-toolchain/gnu-rm
* https://launchpad.net/~team-gcc-arm-embedded/+archive/ubuntu/ppa
