FROM python:3.8
MAINTAINER https://gitlab.com/alelec/docker-arm-none-eabi

RUN apt update && \
    apt install -y gnupg build-essential git git-lfs ca-certificates libltdl-dev \
    ninja-build gcc-multilib pkg-config libffi-dev gcc-mingw-w64 cmake \
    autoconf autotools-dev automake autogen libtool m4 gettext wget qemu-system

# Remove second copy of python, ensure correct version is used
RUN rm /usr/bin/python3

RUN pip install -U pip setuptools wheel pipenv cpp-coveralls black

ARG GCC_ARM_RELEASE=gcc-arm-none-eabi-9-2019-q4-major
ARG GCC_ARM_RELEASE_FILE=${GCC_ARM_RELEASE}-x86_64-linux.tar.bz2
RUN wget --progress=dot:giga https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/RC2.1/${GCC_ARM_RELEASE_FILE}

RUN tar -xjvf ${GCC_ARM_RELEASE_FILE}
RUN rm ${GCC_ARM_RELEASE_FILE}

ENV PATH="/${GCC_ARM_RELEASE}/bin/:${PATH}"

# Print out installed version
RUN VERSION=$(arm-none-eabi-gcc -dumpversion); \
   echo "VERSION=$VERSION"

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Clean up cache
RUN rm -rf /var/cache/apk/*
